# Crumbling Castle

Text-ish game and game engine. Base version implemented in Python, being now ported to Go for cross-platform support
and to be generally more stable.