import curses
import imageio
import json
import numpy as np
import os
import random
import time


"""
TODO:

Add events on dialogue
Add checks on dialogue
Add conditional rendering of text
"""


class Script:

    null_ponderings = [
        "Does she love me? :(",
        "I wonder whether I should change the goddamn job...",
        "I should get the raise.",
        "Mom told me to not study electrical engineering. Mom told me that I should've pursuit governor degree. Why didn't I listen...",
        "God, I hate mondays..."
    ]

    _arithm_ops = {
        '+' : lambda x, y: x + y,
        '-' : lambda x, y: x - y,
        '*' : lambda x, y: x * y,
        '/' : lambda x, y: x / y,
        '//' : lambda x, y: x // y,
        '++' : lambda x, y: str(x) + str(y)
    }

    def run_dialog(self, script):
        if type(script) in (str, dict):
            return script
        else:
            return self.execute_stmt(script)

    def run_description(self, script):
        if type(script) == str:
            return script
        elif type(script) == list:
            return self.execute_stmt(script)

    def ponder(self, ponderance):
        if ponderance:
            return script.execute_stmt(ponderance)
        else:
            return random.choice(self.null_ponderings)

    def execute_stmt(self, stmt, params={}):
        if type(stmt) == str:
            if stmt[0] == "$":
                return game.state[stmt[1:]]
            else:
                return stmt
        elif type(stmt) in (int, float, bool, type(None)):
            return stmt
        elif type(stmt) == list:
            if stmt[0] == "set-flag":
                game.state[self.execute_stmt(stmt[1], params)] = self.execute_stmt(stmt[2], params)
            elif stmt[0] == "if-flag":
                if game.state.get(self.execute_stmt(stmt[1], params)):
                    return self.execute_stmt(stmt[2])
                else:
                    return self.execute_stmt(stmt[3])
            elif stmt[0] == "if-came-from":
                if game.came_from == self.execute_stmt(stmt[1], params):
                    return self.execute_stmt(stmt[2])
                else:
                    return self.execute_stmt(stmt[3])
            elif stmt[0] == "all":
                i = None
                for stmt2 in stmt[1:]:
                    i = self.execute_stmt(stmt2, params)
                return i
            elif stmt[0] == "show-dialog":
                i = self.execute_stmt(stmt[1], params)
                screen.descript(i)
            elif stmt[0] == "if-you-have":
                if stmt[1] in game.inventory:
                    return self.execute_stmt(stmt[2])
                else:
                    return self.execute_stmt(stmt[3])
            elif stmt[0] == "give-item":
                game.inventory.append(stmt[1])
                return self.execute_stmt(stmt[2])
            elif stmt[0] == "arithm":
                op = self._arithm_ops(stmt[2])
                return op(self.execute_stmt(stmt[1]), self.execute_stmt(stmt[3]))
            elif stmt[0] == "first-time":
                if game.state.get(stmt[1]):
                    return self.execute_stmt(stmt[2])
                else:
                    game.state[stmt[1]] = True
                    return self.execute_stmt(stmt[3])
            elif stmt[0] == "chroom":
                game.go_to(stmt[1])
            else:
                raise ValueError("Command {} not found".format(stmt[0]))
        else:
            raise RuntimeError("Non-list script")



class Assets:
    """
    Gathers and processes all assets - level data, images and so on...
    """
    def __init__(self):
        self.assets = {}
        assets = os.listdir("../assets/img")
        for asset in assets:
            path = os.path.join("../assets/img", asset)
            if os.path.isfile(path) and asset.endswith(".bmp"):
                self.assets["img-" + asset.split(".")[0]] = self.load_image(path)

    def get_image(self, name):
        return self.assets["img-" + name]

    def load_image(self, fname):
        a = imageio.imread(fname)
        b = np.zeros(a.shape[:2], np.int8).T
        for ix in range(a.shape[0]):
            for iy in range(a.shape[1]):
                b[iy, ix] = self.colormap(a[ix, iy])
        return b

    def colormap(self, a):
        if a[0] == 170 and a[1] == 0 and a[2] == 0: return 4
        if a[0] == 0 and a[1] == 170 and a[2] == 0: return 5
        if a[0] == 0 and a[1] == 0 and a[2] == 170: return 3
        if a[0] == 170 and a[1] == 0 and a[2] == 170: return 6
        if a[0] == 170 and a[1] == 170 and a[2] == 0: return 8
        if a[0] == 0 and a[1] == 170 and a[2] == 170: return 7
        if a[0] == 0 and a[1] == 0 and a[2] == 0: return 1
        if a[0] == 255 and a[1] == 255 and a[2] == 255: return 2


class Dialog:
    def begin(self, dialogue):
        screen.change_image(assets.get_image(dialogue["img"]))
        screen.refresh()
        self.dialog = dialogue["interaction"]
        self.opt(dialogue["interaction"])

    def opt(self, option):
        if option["ans"] is None:
            txt = script.execute_stmt(option["txt"])
            if txt:
                screen.descript(txt)
            return
        options = {k: v for k, v in option["ans"].items() if type(v) in (dict, str) or
            script.run_dialog(v)}
        options = {k: script.run_dialog(v) for k, v in options.items()}
        options = {k: v for k, v in options.items() if v}
        labels, callbacks = zip(*options.items())
        callbacks = [self.cback(cb) for cb in callbacks]
        selector.question(script.execute_stmt(option["txt"]), labels, callbacks, back=False)

    def signal(self, dialogue):
        pass

    def cback(self, cb):
        if type(cb) == dict:
            return lambda: self.opt(cb)
        elif type(cb) == str:
            return lambda: screen.change_image(assets.get_image(game.room["img"])) or screen.descript(cb)



class Selector:
    def simple_choice(self, labels, callbacks):
        self.previous_scr_mode = screen.current_buffer
        self.previous_controller = controller.current_controller
        self.mode = "simple"
        self.choice = 0
        self.labels = list(labels) + ["-back-"]
        self.callbacks = list(callbacks) + [lambda: screen.descript(script.run_description(game.room["txt"]))]
        screen.set_mode("selector")
        controller.set_mode("selector")

    def question(self, question, labels, callbacks, back="ignoreme"):
        self.previous_scr_mode = screen.current_buffer
        self.previous_controller = controller.current_controller
        self.mode = "question"
        self.question_text = script.execute_stmt(question)
        self.choice = 0
        self.labels = list(labels)
        self.callbacks = list(callbacks)
        screen.set_mode("selector")
        controller.set_mode("selector")
        screen.descript(self.question_text)

    def signal(self, signal):
        if self.mode == "simple" or self.mode == "question":
            if signal == ord("w"):
                self.choice -= 1
                if self.choice < 0:
                    self.choice += len(self.labels)
            elif signal == ord("s"):
                self.choice += 1
                if self.choice >= len(self.labels):
                    self.choice = 0
            elif signal == ord("\n"):
                screen.current_buffer = self.previous_scr_mode
                controller.current_controller = self.previous_controller
                self.callbacks[self.choice]()
                screen.refresh()

    def refresh(self):
        if self.mode == "simple":
            screen.status_win.erase()
            for iy, opt in enumerate(self.labels):
                screen.status_win.addstr(iy, 0, opt, curses.color_pair(10 if self.choice == iy else 9))
            screen.status_win.refresh()
        elif self.mode == "question":
            screen.status_win.erase()
            screen.descript(self.question_text, blink=False)
            for iy, opt in enumerate(self.labels):
                screen.status_win.addstr(12 + iy, 0, opt, curses.color_pair(10 if self.choice == iy else 9))
            screen.status_win.refresh()


class Help:

    help_text = ["The game is steered solely by keyboard. Exit by BACKSPACE",
                 "Use WSAD to steer menus. If action buttons don't work there is no appropriate",
                 " action",
                 "G - go to...",
                 "T - talk to... // interact (to be removed)",
                 "P - ponder",
                 "U - use an item",
                 "ENTER - accept choice",
                 "",
                 "Game autosaves. Due to stability issues, please don't leave during dialogues ;)",
                 "",
                 "Press any key to continue"]

    def __init__(self):
        self.win = curses.newwin(24, 80, 0, 0)

    def refresh(self):
        self.win.erase()
        for iy, opt in enumerate(self.help_text):
            self.win.addstr(iy, 0, opt, curses.color_pair(9))
        self.win.refresh()

    def signal(self, signal):
        screen.set_mode("mainmenu")
        controller.set_mode("mainmenu")


class MainMenu:
    def __init__(self, image="mainmenu"):
        self.image = assets.get_image("mainmenu")
        self.win = curses.newwin(24, 80, 0, 0)
        self.options = ["Play", "Help", "Quit"]
        self.choice = 0

    def refresh(self):
        self.win.erase()
        if self.image is not None:
            for ix in range(self.image.shape[0]):
                for iy in range(self.image.shape[1] - (1 if ix == 79 else 0)):
                    color = int(self.image[ix, iy])
                    try:
                        self.win.addch(iy, ix, ord('x'), curses.color_pair(color))
                    except:
                        raise RuntimeError("X:{}, Y:{}; {}".format(ix, iy, self.image.shape))
        for iy, opt in enumerate(self.options):
            self.win.addstr(20 + iy, 38, opt, curses.color_pair(10 if self.choice == iy else 9))
        self.win.refresh()

    def signal(self, signal):
        if signal == ord("w"):
            self.choice -= 1
            if self.choice < 0:
                self.choice += len(self.options)
        elif signal == ord("s"):
            self.choice += 1
            if self.choice >= len(self.options):
                self.choice = 0
        elif signal == ord("\n"):
            if self.choice == 0:
                game.begin()
            elif self.choice == 1:
                screen.set_mode("help")
                controller.set_mode("help")
            elif self.choice == 2:
                game.runs = False


class ImageView:
    def __init__(self):
        pass

    def signal(self, signal):
        if signal == ord("l"):
            screen.descript(script.run_description(game.room["txt"]))
        if signal == ord("p"):
            screen.descript(script.ponder(game.room.get("ponder")))
        elif signal == ord("g"):
            options = game.room["go-to"]
            labels, targets = zip(*options.items())
            targets = [script.execute_stmt(target) for target in targets]
            selector.simple_choice([label for label, target in zip(labels, targets) if target],
                [self.go_to_callback(target) for target in targets if target]
            )
        elif signal == ord("t"):
            options = game.room.get("characters")
            if options:
                labels, targets = zip(*options.items())
                selector.simple_choice(labels,
                    [self.dialog_callback(target, label) for label, target in zip(labels, targets)]
                )
        elif signal == ord("u"):
            options = game.inventory
            if options:
                selector.simple_choice(options,
                    [self.use_item_callback(target) for target in options]
                )
            else:
                screen.descript("You have no items")


    def go_to_callback(self, target):
        def callable():
            game.go_to(target)
        return callable

    def dialog_callback(self, script, label):
        def callable():
            game.state["last_talker"] = label
            dialog.begin(script)
        return callable

    def use_item_callback(self, item):
        targets = game.items[item]["targets"]
        def callable():
            options = game.room.get("characters")
            options = options if options else []
            options = [x for x in options if x in targets]
            if options:
                selector.simple_choice(["On " + x for x in options],
                    [self.use_callback(item, target) for target in options]
                )
            else:
                screen.descript("There's noone or nothing to use this item on")
        return callable

    def use_callback(self, item, target):
        def callback():
            itemobj = game.items[item]
            for i in itemobj["script"]:
                script.execute_stmt(i, params={"itemname": item, "targetname": target})
        return callback


class Screen:
    def __init__(self):
        self.scr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        curses.start_color()
        curses.raw()
        self.scr.keypad(True)
        self.scr.refresh()
        self.image_win = curses.newwin(24, 50, 0, 0)
        self.status_win = curses.newwin(24, 30, 0, 50)
        self.main_menu = MainMenu()
        self.image_view = ImageView()
        self.help = Help()
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_WHITE)
        curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLUE)
        curses.init_pair(4, curses.COLOR_RED, curses.COLOR_RED)
        curses.init_pair(5, curses.COLOR_GREEN, curses.COLOR_GREEN)
        curses.init_pair(6, curses.COLOR_MAGENTA, curses.COLOR_MAGENTA)
        curses.init_pair(7, curses.COLOR_CYAN, curses.COLOR_CYAN)
        curses.init_pair(8, curses.COLOR_YELLOW, curses.COLOR_YELLOW)
        curses.init_pair(9, curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(10, curses.COLOR_BLACK, curses.COLOR_WHITE)
        self.current_buffer = []

    def set_mode(self, mode):
        """
        Switches between menus and gameplay, remembers last played status lines and so forth
        """
        if mode == "image":
            self.current_buffer = [self.image_win, self.status_win]
        elif mode == "mainmenu":
            self.current_buffer = [self.main_menu]
        elif mode == "help":
            self.current_buffer = [self.help]
        elif mode == "selector":
            self.current_buffer = [selector]
        else:
            raise RuntimeError("Incorrect screen mode")

    def refresh(self):
        [x.refresh() for x in self.current_buffer]

    def deinitialize(self):
        self.scr.keypad(False)
        curses.noraw()
        curses.nocbreak()
        curses.echo()
        curses.endwin()

    def change_image(self, image):
        for ix in range(image.shape[0]):
            for iy in range(image.shape[1] - (1 if ix == 49 else 0)):
                color = int(image[ix, iy])
                try:
                    self.image_win.addch(iy, ix, ord('x'), curses.color_pair(color))
                except:
                    raise RuntimeError("X:{}, Y:{}; {}".format(ix, iy, image.shape))

    def descript(self, text, blink=True):
        words = text.split(' ')
        lines = [""]
        self.status_win.erase()
        while words:
            if len(lines[-1]) + 1 + len(words[0]) <= 30:
                lines[-1] += words[0] + " "
                words = words[1:]
            else:
                lines.append("")
        self.status_win.refresh()
        for ix, line in enumerate(lines):
            if blink:
                for iy, char in enumerate(line[:-1]):
                    time.sleep(0.01)
                    self.status_win.addstr(ix, iy, char)
                    self.status_win.refresh()
            else:
                self.status_win.addstr(ix, 0, line[:-1])

class Controller:
    """
    Controls the input of the game...
    Chooses between mapping of callbacks
    and modes of input and charreading
    """
    def __init__(self):
        self.modes = {}
        self.current_controller = None

    def getch(self):
        time.sleep(0.05)
        ch = screen.scr.getch()
        if(ch == 127):
            self.backspace()
        self.current_controller.signal(ch)

    def set_mode(self, mode):
        self.current_controller = self.modes[mode]

    def backspace(self):
        game.runs = False


class Game:
    def __init__(self):
        self.runs = True
        self.started = False
        with open("../assets/scenario.json") as f:
            self.scenario = json.load(f)
        self.items = self.scenario["items"]
        try:
            with open(".save") as f:
                self.state = json.load(f)
        except:
            self.state = self.scenario["state"].copy()
            self.state['came_from'] = None
            self.state["inventory"] = self.scenario["state"].get("starting_inventory")
            self.state["inventory"] = self.state["inventory"] if self.state["inventory"] else []
        self.inventory = self.state["inventory"]  # non copy
        self.current_room = None

    def loop(self):
        screen.set_mode("mainmenu")
        controller.set_mode("mainmenu")
        screen.refresh()
        while self.runs:
            controller.getch()
            screen.refresh()

    @property
    def came_from(self):
        return self.state['came_from']

    @came_from.setter
    def came_from(self, v):
        self.state['came_from'] = v

    def begin(self):
        self.started = True
        screen.set_mode("image")
        controller.set_mode("image")
        self.go_to(self.state["current_room"])

    def go_to(self, where):
        self.came_from = self.current_room if self.current_room else None
        self.current_room = where
        self.room = self.scenario["rooms"][where]
        screen.change_image(assets.get_image(self.room["img"]))
        screen.refresh()
        screen.descript(script.run_description(self.room["txt"]))

    def save(self):
        if game.started:
            self.state["current_room"] = self.current_room
            self.state["came_from"] = self.came_from
            with open(".save", "w") as f:
                json.dump(self.state, f)


if __name__ == "__main__":
    assets = Assets()
    screen = Screen()
    selector = Selector()
    dialog = Dialog()
    controller = Controller()
    script = Script()
    controller.modes['mainmenu'] = screen.main_menu
    controller.modes['image'] = screen.image_view
    controller.modes['selector'] = selector
    controller.modes['help'] = screen.help
    game = Game()
    try:
        game.loop()
    except Exception as e:
        screen.deinitialize()
        raise e
    else:
        game.save()
        screen.deinitialize()

# Cancer as theme
