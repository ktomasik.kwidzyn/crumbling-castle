# Crumbling Castle

Script language reference:

Behaves like LISP on JSON lists:

- `set-flag <name> <value>` - basic assignment
- `if-flag <flagname> <if_truish> <if_falsish>` - basic control flow
- `if-came-from <where> <if_truish> <if_falsish>` - verifies if you came from given room
- `show-dialog <dialog>` - prints some text
- `if-you-have <what> <if_truish> <if_falsish>` - control flow based on whether you've got an item
- `give-item <what> <msg>` - give item and the return message 
- `all <sl1> <sl2> ...` - execute all sublists - for command composition
- `first-time <flag_name> <descr_if_visited> <descr_if_first_time>` - sets flag upon first visit; room description only
- `arithm <1> <op> <2>` - calculates arithmetic operation between two items (use ++ for string concatenation)

In many cases `$variable_name` if string is interpolated by current value of game state

To do - Krzysztof:
- store of the passer - to buy things from
- inside of the power Plant
- inside of engineer's house
- efun entrance
