Gameplay:

Role-play. Exploration is done with graphics and menus as in the prototype and this is the main plot.
Combat, however, is done in nethack-like interface, where one char == one item/person/place. At the beginning
there will be one combat scene (e.g. rats or something), just to let people learn the system. Later, tactical
and fight decisions may be prevalent, if you choose to be violent. Depends on plot, definitely there is
opportunity to go hostile route.

Some of the scenes will require terminal usage - there will be a little hacking involved.

All is text based, with common set of key-bindings, except for terminal. Controls will be quite
complicated and need to be learned through the gameplay.

There is the save/load option due to the possibility of just dying. Autosave on progress, but manual save
and checkpointing from time to time in specific locations/circumstances.