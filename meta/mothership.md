The mothership is divided into sectors which are grouped into clusters. Clusters are generally far away from each other, with entities called "Hidden Sectors" in between. Those are spaces unreachable for humans and only the Overseer AI controls what is inside. Those are filled with engines, measurement instruments, power generators, air and water creators, basically any machinery that has to work 24/7/365 and needs to be failsafe.
Each cluster has a single Central Sector, which serves the purpose of governing given space. Most of people don't have access to this place, probably a lot of them don't even know about it existing. You can visit other clusters only through the Central Sector magtrain.
Each cluster has couple of Industrial Sectors. Those are semi-automated factories that provide the sector with products that are not necessary for a human being to survive. While they have train connections to other clusters, those only transport the goods and are unsuitable for people.
Each cluster has at least one Living Sector. This is where people live.

The people.
Humans in this world are *werid*. They rarely smile and see the smile as a sign of psychic illnesses. Often you can see people obsessed with a single thing on which they spend all their free time, without ever going out. People getting mad over their passions or drugs are called "NAME". 

The entertainment.
Most of today-known drugs do not exist in the Crumbling Castle. Only one that can be found is morphine, which is rarely sold by medical crew to some shady vendors. VR is very popular and there are some people who are strongly addicted to it, losing the touch with reality. There is a bunch of new substances too. One of which is "The Sun", a very strong and addictive pill that causes controllable hallucinations, something between lucid dreaming and lsd. People tend not to go out and stay in their homes during free time, yet there still are places to socialize, like shops, bars, clubs.
