There's a large large spaceship. It is probably comparable in size to a small moon/asteroid. It is large enough to
be separated to districts, each being its' own smart metropolis with power generations, policies etc.
The spaceship is governed by the high council of engineers and is highly 

People there are ignorant to the technology, with knowledge comparable to laymen of modern times. Tech
is usable, but pragmatic. Very few people understand the spaceship as a larger entity, most people are
specialized only in their most important machinery (if at all). The AI and actual propellants are known
only to the most influential people.

The spaceship was launched by the Earth long long ago. The launch however missed from its destination and currently
is targeted toward complete void. The stars are far and scarce and nuclear power will end some day. Only
the highest command and AI are aware of this. Since light speed limit on communication, the Earth is generally
unreachable by medium and in fact the Earth ceased to respond for the cries for help due to amount of resources
that would be used to drive the people back home.

With that in mind, in order to survive, the highest command plans to limit the energy usage somehow 
and preserve the spaceship from dying out. The space ship may probably hit some star in next several hundreds
of years (it is not certain 100%). There will be some plot that will involve involuntary cryostatis and/or 
genocide.

Game begins when the spaceship's energy generator started to malfunction due to some reason. The main character
is an electrician and is tasked with checking out, what's wrong with the generator (there are of course other
electricians in the area, but you are closest to the generator). You need to enter the main computer hall
to restore the generator to operationability. You were there twice, so you kinda know this place. However, 
after restoring the power, the agent acts peculiar. You restore the power to full operation, but some detail is
off (e.g. total power consumed, ambient temperature, total power produced, oxygen generation, logs are deleted)

