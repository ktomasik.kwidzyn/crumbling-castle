#!/bin/bash

if [ "$1" = "install" ]; then
	go get gopkg.in/yaml.v2
	go get github.com/mitchellh/mapstructure
	go get github.com/gen2brain/raylib-go/raylib
fi

go build -o bin/game
