package main

import "./game"
import "fmt"
import "os"

func gameMain(gameDefPath string) {
	mainMenu := game.CreateMainMenuView()
	helpScreen := game.CreateHelpScreenView()
	locationView := game.CreateLocationView()
	ioc := game.NewIOController()
	gameObject := game.MakeGame(gameDefPath, ioc)
	mainMenu.BindEngine(gameObject.Engine)
	helpScreen.BindEngine(gameObject.Engine)
	locationView.BindEngine(gameObject.Engine)

	ioc.Init()
	ioc.RegisterView("main_menu", mainMenu)
	ioc.RegisterView("help_screen", helpScreen)
	ioc.RegisterView("location_view", locationView)
	ioc.Loop()
}

func main() {
	args := os.Args[1:]
	if len(args) > 0 {
		if args[0] == "maped" {
			if len(args) > 1 {
				mapedMain(args[1])
			} else {
				mapedMain("")
			}
		} else if args[0] == "play" {
			if len(args) > 1 {
				gameMain(args[1])
			} else {
				fmt.Println("Launching the default game")
				gameMain("assets/root.yaml")
			}
		} else {
			fmt.Println("Available options: maped, play [scenario]")
			fmt.Println("No arguments launches a game stored in assets/root.yaml")
		}
	} else {
		gameMain("assets/root.yaml")
	}
}
