package game

import rl "github.com/gen2brain/raylib-go/raylib"
import "fmt"

type Color = rl.Color
type KeyCode = rune // for now, it will change!!

var BLACK Color = rl.Color{0, 0, 0, 255}
var WHITE Color = rl.Color{255, 255, 255, 255}
var RED Color = rl.Color{255, 0, 0, 255}
var GREEN Color = rl.Color{0, 255, 0, 255}
var BLUE Color = rl.Color{0, 0, 255, 255}
var CYAN Color = rl.Color{0, 255, 255, 255}
var MAGENTA Color = rl.Color{255, 0, 255, 255}
var YELLOW Color = rl.Color{255, 255, 0, 255}

type IOController struct {
	runeBuffer        [80][24]rune
	bgBuffer          [80][24]Color
	fgBuffer          [80][24]Color
	views             map[string]View
	viewStack         []View
	fontTexture       *rl.Texture2D
	backgroundTexture *rl.Texture2D
	font              Font
	fontDefinitions   []FontDefinition
}

func (self *IOController) currentView() View {
	return self.viewStack[len(self.viewStack)-1]
}

func (self *IOController) PrintRune(r rune, x int, y int, fg Color, bg Color) {
	if r == self.runeBuffer[x][y] && fg == self.fgBuffer[x][y] && bg == self.bgBuffer[x][y] {
		return
	}
	self.runeBuffer[x][y] = r
	self.fgBuffer[x][y] = fg
	self.bgBuffer[x][y] = bg
}

func (self *IOController) Getch() {
	if rl.IsKeyPressed(rl.KeyEnter) {
		self.currentView().Control(rl.KeyEnter)
		return
	} else if rl.IsKeyPressed(rl.KeyEscape) {
		self.currentView().Control(rl.KeyEscape)
		return
	}
	a := rl.GetKeyPressed()
	if a != -1 {
		self.currentView().Control(a)
	}
}

func (self *IOController) Draw() {
	rl.BeginDrawing()
	rl.ClearBackground(rl.Black)
	for x := 0; x < 80; x++ {
		for y := 0; y < 24; y++ {
			if self.runeBuffer[x][y] > 31 {
				letter, present := self.font[self.runeBuffer[x][y]]
				rl.DrawTexture(*self.backgroundTexture, int32(x*10), int32(y*24), self.bgBuffer[x][y])
				if present {
					rl.DrawTextureRec(*letter.FontTexture, rl.Rectangle{float32((letter.X) * 10), float32(letter.Y) * 24, 10, 24}, rl.Vector2{float32(x * 10), float32(y * 24)}, self.fgBuffer[x][y])
				}
			}
		}
	}
	rl.EndDrawing()
}

func NewIOController() *IOController {
	return &IOController{
		[80][24]rune{},
		[80][24]Color{},
		[80][24]Color{},
		make(map[string]View),
		nil,
		nil,
		nil,
		make(Font),
		make([]FontDefinition, 0)}
}

func (self *IOController) Init() {
	rl.InitWindow(800, 576, "Crumbling Castle")
	rl.SetExitKey(9999)
	y := rl.LoadTextureFromImage(rl.GenImageColor(10, 24, WHITE))
	for _, fontDefinition := range self.fontDefinitions {
		fontTexture := rl.LoadTexture(fontDefinition.FilePath)
		self.registerFont(&fontTexture, fontDefinition.Index)
	}
	self.backgroundTexture = &y
	rl.SetTargetFPS(60)
}

func (self *IOController) RegisterView(name string, view View) {
	view.Bind(self)
	self.views[name] = view
	if len(self.viewStack) == 0 {
		self.PushView(name)
	}
}

func (self *IOController) PushView(name string) {
	self.viewStack = append(self.viewStack, self.views[name])
	self.viewStack[len(self.viewStack)-1].OnEnter()
}

func (self *IOController) PopView() {
	self.viewStack = self.viewStack[:len(self.viewStack)-1]
	self.viewStack[len(self.viewStack)-1].OnEnter()
}

func (self *IOController) ChangeView(name string) {
	self.viewStack[len(self.viewStack)-1] = self.views[name]
	self.viewStack[len(self.viewStack)-1].OnEnter()
}

func (self *IOController) registerFont(fontTexture *rl.Texture2D, startIndex rune) {
	index := startIndex
	if fontTexture.Width != 160 || fontTexture.Height != 144 {
		fmt.Errorf("Invalid fontTexture file - should be 160x144px only.")
	}
	for y := 0; y < 6; y++ {
		for x := 0; x < 16; x++ {
			fmt.Printf("INDEX: %v LETTER:%v\n", index, x)
			self.font[index] = Letter{x, y, fontTexture}
			index++
		}
	}
}

func (self *IOController) PushFont(fontFile string, startIndex rune) {
	self.fontDefinitions = append(self.fontDefinitions, FontDefinition{fontFile, startIndex})
}

func (self *IOController) Loop() {
	for !rl.WindowShouldClose() {
		Refresh(self.currentView())
		self.Draw()
		self.Getch()
	}
}
