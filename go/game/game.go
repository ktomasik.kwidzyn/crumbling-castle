package game

/*
Game aggregates the whole state and controllers of the game
*/

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type Game struct {
	// more game data will be there
	definition map[string]string
	Engine     *ScriptEngine
}

func MakeGame(path string, ioc *IOController) Game {
	var definition map[string]string
	var fonts []map[string]interface{}
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal("Error reading file definition")
	}
	yaml.Unmarshal(bytes, &definition)
	bytes, err = ioutil.ReadFile("./assets/fonts.yaml")
	if err != nil {
		log.Fatal("Error reading file definition")
	}
	yaml.Unmarshal(bytes, &fonts)
	game := Game{definition, nil}
	for _, font := range fonts {
		ioc.PushFont(font["file"].(string), rune(font["index"].(int)))
	}
	engine := NewScriptEngine(&game, ioc)
	game.Engine = &engine
	return game
}

func (self *Game) AddCharacter(character Character) {

}

func (self *Game) AddRoom(room Room) {

}
