package game

import "fmt"

type MainMenu struct {
	sections []*ViewSection
	adapter  *SelectionAdapter
	engine   *ScriptEngine
}

func CreateMainMenuView() View {
	imgSection := NewViewSection(0, 0, 80, 24)
	selectorSection := NewViewSection(38, 12, 6, 5)
	selectionAdapter := NewSelectionAdapter(&selectorSection, []string{
		"Play",
		"Help",
		"Quit",
	}, []interface{}{
		nil_func,
		nil_func,
		nil_func,
	})
	mainMenu := MainMenu{
		make([]*ViewSection, 0),
		&selectionAdapter,
		nil,
	}
	selectionAdapter.callbacks[0] = play(&mainMenu)
	selectionAdapter.callbacks[1] = help(&mainMenu)
	selectionAdapter.callbacks[2] = quit(&mainMenu)
	mainMenu.AddSection(&imgSection)
	mainMenu.AddSection(&selectorSection)
	return &mainMenu
}

func (self *MainMenu) AddSection(viewSect *ViewSection) {
	fmt.Println(len(self.sections))
	fmt.Println(cap(self.sections))
	self.sections = append(self.sections, viewSect)
}

func (self *MainMenu) GetSections() []*ViewSection {
	return self.sections
}

func (self *MainMenu) Message(message string) {
}

func (self *MainMenu) Control(key KeyCode) {
	self.adapter.Control(key)
}

func (self *MainMenu) Bind(ioc *IOController) {
	//fmt.Println(self.sections)
	for _, sec := range self.sections {
		sec.ioc = ioc
	}
}

func (self *MainMenu) BindEngine(engine *ScriptEngine) {
	self.engine = engine
}

func (self *MainMenu) OnEnter() {}

func nil_func() {}

func play(mainMenu *MainMenu) func() {
	return func() {
		mainMenu.engine.Init()
	}
}

func help(mainMenu *MainMenu) func() {
	return func() {
		mainMenu.engine.ioc.PushView("help_screen")
	}
}

func quit(mainMenu *MainMenu) func() {
	return func() {
		mainMenu.engine.TerminateGame()
	}
}
