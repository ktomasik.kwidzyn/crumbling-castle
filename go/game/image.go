package game

type Image struct {
	color  [][]Color
	width  int
	height int
}
