package game

import rl "github.com/gen2brain/raylib-go/raylib"

type Letter struct {
	X           int
	Y           int
	FontTexture *rl.Texture2D
}

type Font = map[rune]Letter

type FontDefinition struct {
	FilePath string
	Index    rune
}
