package game

import rl "github.com/gen2brain/raylib-go/raylib"

type Adapter interface {
	Control(KeyCode)
}

type NullAdapter struct {
}

func (self *NullAdapter) Control(key KeyCode) {

}

type SelectionAdapter struct {
	region    *ViewSection
	labels    []string
	callbacks []func()
	position  int
}

func NewSelectionAdapter(region *ViewSection, labels []string, callbacks []interface{}) SelectionAdapter {
	callbackFuncs := make([]func(), len(callbacks))
	for ix, cb := range callbacks {
		callbackFuncs[ix] = cb.(func())
	}
	selectionAdapter := SelectionAdapter{
		region,
		labels,
		callbackFuncs,
		0,
	}
	(&selectionAdapter).draw()
	return selectionAdapter
}

func (self *SelectionAdapter) draw() {
	self.region.Clear()
	for index, label := range self.labels {
		if index == self.position {
			label := "> " + label
			self.region.DrawString(label, 0, index)
		} else {
			self.region.DrawString(label, 0, index)
		}
	}
}

func (self *SelectionAdapter) Control(key KeyCode) {
	switch key {
	case 'w', 'W', rl.KeyUp:
		self.position -= 1
		if self.position < 0 {
			self.position = len(self.labels) - 1
		}
		self.draw()
	case 's', 'S', rl.KeyDown:
		self.position += 1
		if self.position >= len(self.labels) {
			self.position = 0
		}
		self.draw()
	case rl.KeyEnter:
		self.callbacks[self.position]()
	default:
		// nothing happens
	}
}
