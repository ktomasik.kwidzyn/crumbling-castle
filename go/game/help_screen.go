package game

import (
	"fmt"
	rl "github.com/gen2brain/raylib-go/raylib"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type HelpScreen struct {
	sections []*ViewSection
	engine   *ScriptEngine
	text     string
}

func CreateHelpScreenView() View {
	textSection := NewViewSection(0, 0, 80, 24)
	helpScreen := HelpScreen{
		make([]*ViewSection, 0),
		nil,
		"",
	}
	helpScreen.AddSection(&textSection)
	return &helpScreen
}

func (self *HelpScreen) AddSection(viewSect *ViewSection) {
	self.sections = append(self.sections, viewSect)
}

func (self *HelpScreen) GetSections() []*ViewSection {
	return self.sections
}

func (self *HelpScreen) Message(message string) {
}

func (self *HelpScreen) Control(key KeyCode) {
	if key == rl.KeyEscape {
		self.engine.ioc.PopView()
	}
}

func (self *HelpScreen) Bind(ioc *IOController) {
	fmt.Println(self.sections)
	for _, sec := range self.sections {
		sec.ioc = ioc
	}
}

func (self *HelpScreen) BindEngine(engine *ScriptEngine) {
	self.engine = engine
	hsPath := engine.game.definition["helpScreen"]
	file, err := os.Open(hsPath)
	if err != nil {
		fmt.Println(err)
		fmt.Println(hsPath)
		log.Fatal("Cannot open the helpscreen file")
	}
	defer file.Close()
	bytes, err := ioutil.ReadAll(file)
	self.text = string(bytes)
}

func (self *HelpScreen) OnEnter() {
	lines := strings.Split(self.text, "\n")
	for index, line := range lines {
		self.sections[0].DrawString(line, 0, index)
	}
}
