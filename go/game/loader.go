package game

import "encoding/json"
import "github.com/mitchellh/mapstructure"
import "image"
import _ "image/png"
import "os"
import "log"

func (self *Game) LoadFromJSON(content []byte) {
	var data map[string]interface{}
	json.Unmarshal(content, &data)
	for _, room := range data["rooms"].([]interface{}) {
		var object Room
		mapstructure.Decode(room, &object)
		self.AddRoom(object)
	}
	for _, chr := range data["characters"].([]interface{}) {
		var object Character
		mapstructure.Decode(chr, &object)
		self.AddCharacter(object)
	}
}

func LoadImage(filename string) image.Image {
	path := "assets/" + filename
	reader, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer reader.Close()
	m, _, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}
	return m
}
