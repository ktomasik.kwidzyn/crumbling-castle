package game

import "fmt"
import rl "github.com/gen2brain/raylib-go/raylib"

type LocationView struct {
	location *Location
	gotoList []string
	sections []*ViewSection
	engine   *ScriptEngine
	action   int
	menuPos  int
}

func CreateLocationView() View {
	messageSection := NewViewSection(50, 0, 30, 18)
	menuSection := NewViewSection(50, 18, 30, 6)
	imageSection := NewViewSection(0, 0, 50, 24)
	//windows := nil // to add windows later, e.g. equipment window
	locationView := LocationView{
		nil,
		nil,
		make([]*ViewSection, 0),
		nil,
		ACTION_DEFAULT,
		0}
	locationView.AddSection(&messageSection)
	locationView.AddSection(&menuSection)
	locationView.AddSection(&imageSection)
	return &locationView
}

func (self *LocationView) AddSection(viewSect *ViewSection) {
	self.sections = append(self.sections, viewSect)
}

func (self *LocationView) GetSections() []*ViewSection {
	return self.sections
}

func (self *LocationView) Message(message string) {
	//self.sections[0].DrawMessage(message)
}

func (self *LocationView) Control(key KeyCode) {
	switch self.action {
	case ACTION_GOTO:
		self.gotoControl(key)
		break
	default:
		self.defaultControl(key)
	}
	self.refresh()
}

func (self *LocationView) defaultControl(key KeyCode) {
	switch key {
	case 'p', 'P':
		self.menuPos = 0
		if self.action == ACTION_DEFAULT {
			self.action = ACTION_PONDER
		} else if self.action == ACTION_PONDER {
			self.action = ACTION_DEFAULT
		}
		break
	case 'g', 'G':
		self.gotoList = self.location.GetGotoList(self.engine)
		self.menuPos = 0
		self.action = ACTION_GOTO
		break
	case 'i', 'I':
		//TODO: Interact / Talk
		break
	case 'u', 'U':
		//TODO: Use an item
		break
	case 'e', 'E':
		//TODO: Equipment screen
		break
	case rl.KeyEscape:
		self.engine.ioc.PopView()
		break
	default:
		self.menuPos = 0
		if self.action == ACTION_PONDER {
			self.action = ACTION_PONDER
		} else {
			self.action = ACTION_DEFAULT
		}
	}
}

func (self *LocationView) gotoControl(key KeyCode) {
	switch key {
	case rl.KeyEscape:
		self.action = ACTION_DEFAULT
		break
	case 's', 'S', rl.KeyDown:
		if self.menuPos < len(self.gotoList)-1 {
			self.menuPos += 1
		}
		break
	case 'w', 'W', rl.KeyUp:
		if self.menuPos > 0 {
			self.menuPos -= 1
		}
		break
	case rl.KeyEnter:
		self.action = ACTION_DEFAULT
		self.engine.GoToRoom(self.gotoList[self.menuPos])
		//Change Room to self.location.GotoList[self.menuPos]
	}
}

func (self *LocationView) Bind(ioc *IOController) {
	fmt.Println(self.sections)
	for _, sec := range self.sections {
		sec.ioc = ioc
	}
}

func (self *LocationView) BindEngine(engine *ScriptEngine) {
	self.engine = engine
}

func (self *LocationView) refresh() {
	// This should be changed to apprioriate adapters...
	self.location = self.engine.CurrentLocation()

	for _, section := range self.sections {
		section.Clear()
	}

	self.sections[0].DrawMessage(self.location.descr)
	self.sections[2].DrawGraphics(self.location.image, 0, 0)

	if self.action == ACTION_GOTO {
		self.sections[1].DrawString("Go to:", 0, 0)
		for i, v := range self.gotoList {
			if i == self.menuPos {
				self.sections[1].DrawString(">", 0, i+1)
			}
			self.sections[1].DrawString(v, 1, i+1)
		}
	} else if self.action == ACTION_PONDER {
		self.sections[0].Clear()
		self.sections[0].DrawMessage(self.location.ponder)
	} else {
		self.sections[1].Clear()
	}
}

func (self *LocationView) OnEnter() {
	self.refresh()
}
