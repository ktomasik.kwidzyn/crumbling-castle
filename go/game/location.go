package game

import "image"

type Location struct {
	descr    string
	ponder   string
	image    image.Image
	gotoList []interface{}
	scripts  map[string]SValue
}

func (self *Location) GetGotoList(engine *ScriptEngine) []string {
	gotoList := make([]string, len(self.gotoList))
	i := 0
	for _, v := range self.gotoList {
	    switch v.(type) {
	    	case string:
				gotoList[i] = v.(string)
				i += 1
	    	case []interface{}:
	    		returnable := engine.VMRun(v.([]interface{}))
	    		// TODO: when the go-to is changed, this should be modified
	    		switch returnable.(type){
	    			case String:
		    			gotoList[i] = string(returnable.(String))
		    			i += 1
	    		}
	    }
	}
	return gotoList[:i]
}

func LocationFromDescription(engine *ScriptEngine, spec map[interface{}]interface{}) *Location {
	image := LoadImage(spec["image"].(string))
	ponderText := spec["ponder"].(string)
	if ponderText == "default" {
		ponderText = "This is default ponder text."
	}
	gotoListInterface := spec["goto"].([]interface{})
	scriptsValue := spec["hooks"]
	scriptsVal := make(map[string]SValue)
	if scriptsValue != nil {
		for k, v := range scriptsValue.(map[interface{}]interface{}) {
			scriptsVal[k.(string)] = v.(SValue)
		}
	}
	location := Location{
		spec["descr"].(string),
		ponderText,
		image,
		gotoListInterface,
		scriptsVal}
	return &location
}
