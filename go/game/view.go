package game

type View interface {
	AddSection(*ViewSection)
	GetSections() []*ViewSection
	Message(string)
	Control(KeyCode) // for passing control codes and execution of logic
	Bind(*IOController)
	BindEngine(*ScriptEngine) // for passing a reference to scripting engine
	OnEnter()
}

func Refresh(self View) {
	for _, sec := range self.GetSections() {
		sec.Refresh()
	}
}
