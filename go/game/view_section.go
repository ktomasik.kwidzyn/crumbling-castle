package game

import "strings"
import rl "github.com/gen2brain/raylib-go/raylib"
import "image"

//"image/png" has to be imported in order for image.Decode to decode png images properly. If we wanted to use jpg, we'd need to import "image/jpeg"

type ViewSection struct {
	ioc           *IOController
	width         int
	height        int
	x             int
	y             int
	buffer        [][]rune
	bgColorBuffer [][]Color
	fgColorBuffer [][]Color
	bgColor       Color
	fgColor       Color
}

func NewViewSection(x int, y int, width int, height int) ViewSection {
	buffer := make([][]rune, width)
	for i := 0; i < width; i++ {
		buffer[i] = make([]rune, height)
	}
	bgColor := make([][]Color, width)
	for i := 0; i < width; i++ {
		bgColor[i] = make([]Color, height)
	}
	fgColor := make([][]Color, width)
	for i := 0; i < width; i++ {
		fgColor[i] = make([]Color, height)
	}
	viewSection := ViewSection{
		nil,
		width,
		height,
		x,
		y,
		buffer,
		bgColor,
		fgColor,
		BLACK,
		WHITE,
	}
	viewSection.Clear()
	return viewSection
}

func (self *ViewSection) PrintRune(r rune, x int, y int, fgColor Color, bgColor Color) {
	self.buffer[x][y] = r
	self.fgColorBuffer[x][y] = fgColor
	self.bgColorBuffer[x][y] = bgColor
}

func (self *ViewSection) Clear() {
	for i := 0; i < self.width; i++ {
		for j := 0; j < self.height; j++ {
			self.PrintRune(' ', i, j, self.bgColor, self.bgColor)
		}
	}
}

func (self *ViewSection) DrawString(text string, x int, y int) {
	for ix, chr := range text {
		self.PrintRune(chr, x+ix, y, self.fgColor, self.bgColor)
	}
}

func (self *ViewSection) DrawMessage(text string) {
	self.Clear()
	words := strings.Fields(text)
	lines := make([]string, self.width)
	lineIndex := 0
	currentString := ""
	for _, word := range words {
		if len(currentString)+1+len(word) < self.width {
			currentString = currentString + " " + word
		} else {
			lines[lineIndex] = currentString
			lineIndex += 1
			currentString = word
		}
	}
	if len(currentString) > 0 {
		lines[lineIndex] = currentString
	}
	for ix, line := range lines {
		self.DrawString(line, 0, ix)
	}
}

func (self *ViewSection) DrawImage(image Image, x int, y int) {
	for i := 0; i < image.width; i++ {
		for j := 0; j < image.height; j++ {
			self.PrintRune(' ', x+i, y+j, image.color[i][j], image.color[i][j])
		}
	}
}

func (self *ViewSection) DrawGraphics(m image.Image, xPos int, yPos int) {
	//xMax and yMax args serve the purpose of limiting the image to fit the view_section bounds.
	var xBound, yBound int
	bounds := m.Bounds()
	xBound, yBound = bounds.Max.X-bounds.Min.X+xPos, bounds.Max.Y-bounds.Min.Y+yPos
	for y := xPos; y < xBound; y++ {
		for x := yPos; x < yBound; x++ {
			printedRune := ' '
			r, g, b, a := m.At(x, y).RGBA()
			r, g, b, a = r>>15, g>>15, b>>15, a>>13
			switch a {
			case 1:
				printedRune = 128
			case 2:
				printedRune = 129
			case 3:
				printedRune = 130
			}
			self.PrintRune(printedRune, x, y, rl.Color{uint8(r), uint8(g), uint8(b), 255}, rl.Color{uint8(r * 255), uint8(g * 255), uint8(b * 255), 255})
			// A color's RGBA method returns values in the range [0, 65535].
			// Shifting by 15 reduces this to the range [0, 1].
			// MAYBE: Shift a for 13 instead, get [0, 4] range and use " ", "▒", "▓", "█" as alpha channel
		}
	}
	/*
		  image, err := ioutil.ReadFile(path)
			if err != nil {
				log.Fatal(err)
			}
			r, g, b, a := image.At(1, 1).RGBA()
			fmt.Println(r)
	*/
}

func (self *ViewSection) DrawImageFromFile(filename string, xPos int, yPos int) {
	m := LoadImage(filename)
	self.DrawGraphics(m, xPos, yPos)
}

func (self *ViewSection) Refresh() {
	for i := 0; i < self.width; i++ {
		for j := 0; j < self.height; j++ {
			self.ioc.PrintRune(self.buffer[i][j], self.x+i, self.y+j, self.fgColorBuffer[i][j], self.bgColorBuffer[i][j])
		}
	}
}
