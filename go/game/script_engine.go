package game

/*
Central entrypoint for the control of the game from various points of the codebase
Should allow for total scripting of the engine
*/

import (
	//	"bufio"
	"encoding/json"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path"
)

var debug = true

type SValue interface{}

type Num float64
type String string
type List []SValue
type Map map[string]SValue
type Error string
type Method func(*ScriptEngine, []SValue) SValue
type Null struct{}

func (self String) unbox() string {
	return string(self)
}

func convert(data interface{}) SValue {
	switch data.(type) {
	case bool:
		data, _ := data.(bool)
		if data {
			return Num(1)
		} else {
			return Num(0)
		}
	case float64:
		return Num(data.(float64))
	case string:
		return String(data.(string))
	case []interface{}:
		mySlice := make(List, len(data.([]interface{})))
		for index, item := range data.([]interface{}) {
			mySlice[index] = convert(item)
		}
		return mySlice
	case map[string]interface{}:
		return Error("Not implemented yet")
	case nil:
		return Null{}
	default:
		panic("Convert failed")
	}
}

////////////////////////////////////////////////////////////////
// Engine class

type ScriptEngine struct {
	game      *Game
	ioc       *IOController
	state     map[string]SValue
	frame     [](map[string]SValue)
	builtins  map[string]Method
	running   bool
	scenario  map[string]interface{}
	locations map[string](*Location)
}

func NewScriptEngine(game *Game, ioc *IOController) ScriptEngine {
	stateMap, builtins := make(map[string]SValue), map[string]Method{
		"print":    (*ScriptEngine).printValue, // TODO change to log
		"quit":     (*ScriptEngine).shutDown,
		"+":        (*ScriptEngine).add,
		"-":        (*ScriptEngine).sub,
		"*":        (*ScriptEngine).mul,
		"/":        (*ScriptEngine).div,
		"if":       (*ScriptEngine).ifThenElse,
		"quote":    (*ScriptEngine).quote,
		"verbatim": (*ScriptEngine).quoteItem,
		"list":     (*ScriptEngine).makeList,
		"set":      (*ScriptEngine).setValue,
		"def":      (*ScriptEngine).setGlobal,
		"get":      (*ScriptEngine).getValue,
		"getGlob":  (*ScriptEngine).getGlobalValue,
		"import":   (*ScriptEngine).libraryFromAssets,
		"slice":    (*ScriptEngine).slice,
		"cat":      (*ScriptEngine).cat,
		"struct":   (*ScriptEngine).makeStruct,
		"getattr":  (*ScriptEngine).getAttr,
		"setattr":  (*ScriptEngine).setAttr,
		// loadJSON
		// saveJSON
		// interpolate - replaces ${NAME} with values from current scope
		// replace - replaces X for Y in a string
		// TODO: ":abc" should return string abc - this is quoting
		// TODO: "print" should evaluate things
		// each - run multiple commands, return last value
		// eachGather - 
		// map - maps a list of object through a function (1 or 2 argument respectively)
		// while - a condition loop
		// convert - casts items between types: map to list, list to str, str to num, num to str etc.
	}
	return ScriptEngine{game, ioc, stateMap, make([](map[string]SValue), 0), builtins,
		true, make(map[string]interface{}), make(map[string](*Location))}
}

func (self *ScriptEngine) LoadJSON(path string) {
	panic("Load JSON not implemented")
}

func (self *ScriptEngine) TerminateGame() {
	// this should save game
	// this should then kill the game
	os.Exit(0)
}

func (e *ScriptEngine) get(key string) SValue {
	ix := len(e.frame)
	if ix > 0 {
		for i := ix - 1; ix >= 0; i-- {
			if val, ok := e.frame[i][key]; ok {
				return val
			}
		}
	}
	if val, ok := e.state[key]; ok {
		return val //do something here
	} else {
		if val, ok := e.builtins[key]; ok {
			return val //ensure that the type is proper
		} else {
			panic("Cannot find proper key in the frame")
		}
	}
}

func (e *ScriptEngine) getGlobal(key string) SValue {
	if val, ok := e.state[key]; ok {
		return val //do something here
	} else {
		panic("Cannot find proper key in the frame")
	}
}

func (engine *ScriptEngine) evalTree(expr SValue) SValue {
	if debug {
		fmt.Print("Evaluating: ")
		fmt.Println(expr)
	}
	switch expr.(type) {
	case Num, String, Map, Error, Null:
		return expr
	case List:
		key := engine.evalTree(expr.(List)[0])
		switch key.(type) {
		case String:
			callable := engine.get(key.(String).unbox()) //get function from state, and if not, from builtins
			switch callable.(type) {
			case Method:
				args := expr.(List)[1:]
				return (callable.(Method))(engine, args)
			case List:
				args := expr.(List)[1:]
				return engine.callList(callable.(List), args)
			default:
				panic("Not implemented error") // pass
			}
		default:
			panic("Non-string index to map of functions")
		}
	default:
		panic("Incompatible type in SValue evaluation")
	}
}

func (engine *ScriptEngine) evalCode(text string) {
	var data interface{}
	json.Unmarshal([]byte(text), &data)
	ast := convert(data)
	engine.evalTree(ast)
}

func (e *ScriptEngine) callList(list List, args List) SValue {
	params := list[0].(List)
	var options Map
	switch list[1].(type){
		case Map:
		options = list[1].(Map)
	}
	body := list[2:]
	isMacro, _ := options["macro"]
	if isMacro == nil {
		newArgs := make(List, len(args))
		for ix, x := range args {
			newArgs[ix] = e.evalTree(x)
		}
		args = newArgs
	}
	e.pushFrame()
	for ix, key := range params {
		e.set(key.(string), args[ix])
	}
	var retVal SValue
	for _, subexpr := range body {
		retVal = e.evalTree(subexpr)
	}
	e.popFrame()
	return retVal
}

func (e *ScriptEngine) pushFrame() {
	frame := make(map[string]SValue)
	e.frame = append(e.frame, frame)
}

func (e *ScriptEngine) popFrame() {
	if len(e.frame) > 0 {
		e.frame = e.frame[0 : len(e.frame) - 1]
	} else {
		panic("Nothing to pop")
	}
}

func (e *ScriptEngine) set(key string, value SValue) {
	if len(e.frame) > 0 {
		e.frame[len(e.frame) - 1][key] = value
	} else {
		e.state[key] = value
	}
}

func (e *ScriptEngine) define(key string, value SValue) {
	// for constants
	e.state[key] = value
}


////////////////////////////////////////////////////////////////
// Functions included in the script

func (e *ScriptEngine) printValue(args []SValue) SValue {
	fmt.Println(e.evalTree(args[0]))
	return args[0]
}

func (e *ScriptEngine) libraryFromAssets(args []SValue) SValue {
	libPath := e.evalTree(args[0]).(string)
	data, isOk := ioutil.ReadFile(path.Join("assets", libPath))
	if isOk != nil {
		panic("Dynamic library load failed")
	}
	e.evalCode(string(data))
	return Null{}
}

func (e *ScriptEngine) shutDown(_ []SValue) SValue {
	e.running = false
	return Num(0)
}

func (e *ScriptEngine) ifThenElse(args []SValue) SValue {
	var test bool
	value := e.evalTree(args[0])
	switch value.(type) {
		case Null:
			test = false
		case String:
			test = len(string(value.(String))) > 0
		case Num:
			test = float64(value.(Num)) != 0
		case List:
			test = len(value.(List)) > 0
		case Map:
			test = len(value.(Map)) > 0
		case Error:
			test = true
		case Method:
			test = true
		default:
			panic("ifThenElse - bad type")
	}
	if test {
		return e.evalTree(args[1])
	} else {
		if len(args) > 2 {
			return e.evalTree(args[2])
		}
		return Null{}
	}
}

func (e *ScriptEngine) quote(args []SValue) SValue {
	return args
}

func (e *ScriptEngine) quoteItem(args []SValue) SValue {
	return args[0]
}

func (e *ScriptEngine) makeList(args []SValue) SValue {
	myList := make([]SValue, len(args))
	for ix, val := range args {
		myList[ix] = e.evalTree(val)
	}
	return myList
}

func (e *ScriptEngine) setValue(args []SValue) SValue {
	val := e.evalTree(args[1])
	e.set(string(e.evalTree(args[0]).(String)), val)
	return val
}

func (e *ScriptEngine) setGlobal(args []SValue) SValue {
	val := e.evalTree(args[1])
	e.define(string(e.evalTree(args[0]).(String)), val)
	return val
}

func (e *ScriptEngine) getValue(args []SValue) SValue {
	return e.get(string(e.evalTree(args[0]).(String)))
}

func (e *ScriptEngine) add(args []SValue) SValue {
	newArgs := make(List, len(args))
	for index, item := range args {
		newArgs[index] = e.evalTree(item)
	}
	num := Num(0)
	for _, item := range newArgs {
		num = num + item.(Num)
	}
	return num
}

func (e *ScriptEngine) sub(results []SValue) SValue {
	if len(results) == 2 {
		return Error("Overcomplete list for subtraction")
	}
	return e.evalTree(results[0]).(Num) - e.evalTree(results[1]).(Num)
}

func (e *ScriptEngine) mul(args []SValue) SValue {
	newArgs := make(List, len(args))
	for index, item := range args {
		newArgs[index] = e.evalTree(item)
	}
	num := Num(0)
	for _, item := range newArgs {
		num = num * item.(Num)
	}
	return num
}

func (e *ScriptEngine) div(results []SValue) SValue {
	if len(results) == 2 {
		return Error("Overcomplete list for division")
	}
	return e.evalTree(results[0]).(Num) / e.evalTree(results[1]).(Num)
}

func (e *ScriptEngine) slice(args []SValue) SValue {
	var from int
	var end int
	from = int(e.evalTree(args[1]).(Num))
	end = int(e.evalTree(args[2]).(Num))
	var mySlice SValue
	switch args[0].(type) {
		case List:
		mySlice = args[0].(List)[from : end]
		case String:
		mySlice = args[0].(String)[from : end]
		default:
		panic("Incorrect type")
	}
	return mySlice
}

func (e *ScriptEngine) cat(args []SValue) SValue {
	// TODO - add strings and structs
	head := e.evalTree(args[0])
	for _, tail := range args[1:]{
		tail = e.evalTree(tail)
		head = append(head.(List), (tail.(List))...)
	}
	return head
}

func (e *ScriptEngine) getGlobalValue(args []SValue) SValue {
	key := e.evalTree(args[0])
	return e.getGlobal(key.(string))
}

func (e *ScriptEngine) makeStruct(args []SValue) SValue {
	newOne := make(Map)
	for len(args) > 1 {
		newOne[e.evalTree(args[0]).(string)] = args[1]
		args = args[2:]
	}
	return newOne
}

func (e *ScriptEngine) getAttr(args []SValue) SValue {
	return e.evalTree(args[0]).(Map)[e.evalTree(args[1]).(string)]
}

func (e *ScriptEngine) setAttr(args []SValue) SValue {
	value := e.evalTree(args[2])
	e.evalTree(args[0]).(Map)[e.evalTree(args[1]).(string)] = value
	return value
}

// game scripting:

// setImage
// message
// setScenePortrait
// setCharacterPortrait
// goToRoom
// jumpTo
// buildMenu - allows to filter options

func (self *ScriptEngine) LoadScenario(path string) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		panic("Error reading scenario definition")
	}
	yaml.Unmarshal(bytes, &self.scenario)
	for key, location := range self.scenario["locations"].(map[interface{}]interface{}) {
		descr := location.(map[interface{}]interface{})
		self.locations[key.(string)] = LocationFromDescription(self, descr)
	}
	self.GoToRoom(self.scenario["starting_location"].(string))
	self.ioc.PushView(self.scenario["main_view"].(string))
	if self.scenario["on_load"] != nil {
		self.evalTree(convert(self.scenario["on_load"]))
	}
}

func (self *ScriptEngine) Init() {
	saveFile := self.game.definition["saveFile"]
	scenarioFile := self.game.definition["scenarioFile"]
	if _, err := os.Stat(saveFile); err == nil {
		panic("Deserialization of a save file to be done")
	} else {
		self.LoadScenario(scenarioFile)
	}
}

func (self *ScriptEngine) GoToRoom(where string) {
	self.state["current_location"] = SValue(where)
	script := self.CurrentLocation().scripts["onEnter"]
	fmt.Print(where, script)
	var value SValue
	switch script.(type) {
		case []interface{}:
		value = self.evalTree(convert(script))
		case List:
		value = self.evalTree(script.(List))
	}
	switch value.(type) {
		case Map:
		self.locations[self.state["current_location"].(string)] = LocationFromDescription(self, 
			value.(map[interface{}]interface{}))
	}
}

func (self *ScriptEngine) CurrentLocation() *Location {
	var location_string string
	location_obj := self.state["current_location"]
    switch location_obj.(type){
    	case string:
    	location_string = location_obj.(string)
    	case List:
    	result := self.evalTree(location_obj)
    	location_string = result.(string)  // might fail
    	default:
    	panic("Wrong location type")
    }
	location := self.locations[location_string]
	return location
}

func (self *ScriptEngine) VMRun(code []interface{}) SValue {
    var ast SValue
    ast = convert(code)
    retValue := self.evalTree(ast)
	return retValue
}
