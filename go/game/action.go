package game

const (
	ACTION_DEFAULT   = iota
	ACTION_GOTO      = iota
	ACTION_PONDER    = iota
	ACTION_USE       = iota
	ACTION_EQUIPMENT = iota
)
