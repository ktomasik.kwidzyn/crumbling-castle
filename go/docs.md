Generally, `assets/root.yaml` is a file loaded on a game/per/game basis and contains
absolutely core definitions of where to find scenarios, save files and additional definitions.
This file is used to add non-standard views and to mod it.

On Play, one of two files is searched for:

- scenario file
- save file

Each file serialized the game state. They both refer to baseline resources files that define interactions
and characters and so on, that provide "prototypes". SCenario and save files only encapsulate the state of the game
- instances of characters that already exist, items you have, variables and so on. They are used with
the resource files to fully reconstruct the game state.

Resource files will be written probably in LUA. Engine script will use LUA and JSON/YAML.
JSON is use for serialization and LUA for proper scripting. Both engines will use the same namespace.

